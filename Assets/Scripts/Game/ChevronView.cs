﻿using UnityEngine;

public class ChevronView : MonoBehaviour
{
    [SerializeField] private Transform transformCached;
    [SerializeField] private Animation animation;
    [SerializeField] private AnimationClip animationClip;

    public void PlayAnimationFrom(float time)
    {
        animation[animationClip.name].time = time;
        animation.Play();
    }

    public float GetClipLength()
    {
        return animation[animationClip.name].length;
    }

    public float GetDistance(Vector2 position)
    {
        return Vector2.Distance(position, transformCached.position);
    }
}