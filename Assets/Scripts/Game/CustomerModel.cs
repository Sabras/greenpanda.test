﻿using System;
using System.Collections;

public class CustomerModel
{
    public Action PizzaConsumed;

    public IEnumerator EatPizza(PizzaController pizzaController)
    {
        yield return pizzaController.AnimateEating();
        GlobalEventsHolder.PizzaConsumed?.Invoke(pizzaController);
        PizzaConsumed?.Invoke();
    }
}