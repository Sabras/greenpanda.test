﻿using UnityEngine;

public class ChefView : MonoBehaviour
{
    private const string CookingAnimationTrigger = "Cooking";

    [SerializeField] private Animator animator;

    private void OnEnable()
    {
        GlobalEventsHolder.SpawnPizza += () => animator.SetTrigger(CookingAnimationTrigger);
    }
}