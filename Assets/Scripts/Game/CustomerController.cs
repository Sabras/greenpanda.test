﻿using UnityEngine;

public class CustomerController
{
    private CustomerView view;
    private CustomerModel model;

    private bool busy;

    public CustomerController(CustomerView view, CustomerModel model)
    {
        this.view = view;
        this.model = model;
        view.CollisionEvent += CheckPizzaCollision;
        model.PizzaConsumed += FreeCustomer;
    }

    private void CheckPizzaCollision(Collider2D collider)
    {
        if (busy) return;
        if (collider.gameObject.CompareTag("Pizza"))
        {
            var pizzaView = collider.GetComponent<PizzaView>();
            if (pizzaView)
            {
                busy = true;
                var pizzaController = pizzaView.PizzaController;
                pizzaController.SetParent(view.transform);
                view.AnimateEating();
                view.StartCoroutine(model.EatPizza(pizzaView.PizzaController));
            }
        }
    }

    private void FreeCustomer()
    {
        busy = false;
    }
}