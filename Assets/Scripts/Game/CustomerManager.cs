﻿using UnityEngine;

public class CustomerManager : MonoBehaviour
{
    [SerializeField] private CustomerView testCustomerView;

    private void Start()
    {
        InitCustomer();
    }

    private void InitCustomer()
    {
        var customerModel = new CustomerModel();
        var customerController = new CustomerController(testCustomerView, customerModel);
    }
}