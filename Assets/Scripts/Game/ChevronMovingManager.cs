﻿using UnityEngine;

public class ChevronMovingManager : MonoBehaviour
{
    [SerializeField] private ChevronView[] chevrons;

    private void Start()
    {
        var launchTime = 0f;
        var timeBetween = chevrons[0].GetClipLength() / chevrons.Length;

        for (int i = 0; i < chevrons.Length; i++)
        {
            chevrons[i].PlayAnimationFrom(launchTime);
            launchTime += timeBetween;
        }
    }
}