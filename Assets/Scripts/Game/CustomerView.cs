﻿using System;
using UnityEngine;

public class CustomerView : MonoBehaviour
{
    public const string EatingAnimationKey = "Eating";

    [SerializeField] private ColliderDetector2D colliderDetector;
    [SerializeField] private Animator animator;

    public Action<Collider2D> CollisionEvent;

    private void OnEnable()
    {
        colliderDetector.TriggerEvent += Collision;
    }

    private void Collision(Collider2D collider)
    {
        CollisionEvent?.Invoke(collider);
    }

    public void AnimateEating()
    {
        animator.SetTrigger(EatingAnimationKey);
    }
}