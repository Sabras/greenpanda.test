﻿using System.Collections;
using UnityEngine;

public class PizzaView : MonoBehaviour
{
    public const string DisappearAnimationKey = "PizzaDisappear";

    public PizzaController PizzaController { get; set; }

    [SerializeField] private Transform transformCached;
    [SerializeField] private ColliderDetector2D colliderDetector;
    [SerializeField] private Animator animator;
    [SerializeField] private AnimationClip appearAnimationClip;
    [SerializeField] private AnimationClip disappearAnimationClip;

    private float readyTimer;

    private void OnEnable()
    {
        colliderDetector.TriggerEvent += SetPizzaOnTrack;
    }

    private void Update()
    {
        readyTimer += Time.time;

        if (readyTimer > appearAnimationClip.length && PizzaController.State == PizzaState.Appearing)
        {
            PizzaController.State = PizzaState.Ready;
        }
    }

    public IEnumerator DisappearPizzaAnimation()
    {
        PizzaController.State = PizzaState.Disappearing;
        animator.SetTrigger(DisappearAnimationKey);
        yield return new WaitForSeconds(disappearAnimationClip.length);
    }

    public void SetParent(Transform parent)
    {
        transformCached.SetParent(parent, true);
    }

    private void SetPizzaOnTrack(Collider2D col)
    {
        if (PizzaController.State != PizzaState.Ready) return;
        if (col.gameObject.CompareTag("Chevron"))
        {
            transformCached.SetParent(col.transform, true);
        }
    }
}