﻿using UnityEngine;
using UnityEngine.UI;

public class PizzaFactory : MonoBehaviour
{
    [SerializeField] private PizzaView pizzaPrefab;
    [SerializeField] private Transform pizzaSpawnTransform;
    [SerializeField] private Button spawnPizzaButton;

    private float spawnTimer;
    private float blockSpawnTimer;

    private void OnEnable()
    {
        spawnPizzaButton.onClick.AddListener(SpawnPizza);
        GlobalEventsHolder.PizzaConsumed += DestroyPizza;
        spawnTimer = Settings.Instance.PizzaSpawnDelay;
    }

    private void Update()
    {
        if (blockSpawnTimer > 0)
        {
            blockSpawnTimer -= Time.deltaTime;
            return;
        }

        spawnTimer += Time.deltaTime;
        if (spawnTimer > Settings.Instance.PizzaSpawnDelay)
        {
            SpawnPizza();
            spawnTimer = 0;
        }
    }

    private void SpawnPizza()
    {
        if (blockSpawnTimer > 0) return;
        blockSpawnTimer = Settings.Instance.PizzaSpawnBlockTimer;
        var pizzaView = Instantiate(pizzaPrefab, pizzaSpawnTransform);
        var pizzaModel = new PizzaModel();
        var pizzaController = new PizzaController(pizzaView, pizzaModel);
        pizzaView.PizzaController = pizzaController;
        GlobalEventsHolder.SpawnPizza?.Invoke();
    }

    private void DestroyPizza(PizzaController pizzaController)
    {
        pizzaController.Destroy();
    }
}