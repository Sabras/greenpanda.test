﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

public class PizzaController
{
    private PizzaView view;
    private PizzaModel model;

    public PizzaState State;

    public PizzaController(PizzaView view, PizzaModel model)
    {
        this.view = view;
        this.model = model;
        model.PizzaScore = Settings.Instance.PizzaScore;
    }

    public int GetPizzaScore()
    {
        return model.PizzaScore;
    }

    public IEnumerator AnimateEating()
    {
        State = PizzaState.Disappearing;
        yield return view.DisappearPizzaAnimation();
    }

    public void Destroy()
    {
        model = null;
        Object.Destroy(view.gameObject);
    }

    public void SetParent(Transform parent)
    {
        view.SetParent(parent);
    }
}

public enum PizzaState
{
    Appearing,
    Ready,
    Disappearing
}