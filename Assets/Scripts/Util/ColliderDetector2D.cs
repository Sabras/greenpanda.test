﻿using System;
using UnityEngine;

public class ColliderDetector2D : MonoBehaviour
{
    public Action<Collision2D> CollisionEvent;
    public Action<Collider2D> TriggerEvent;

    private void OnCollisionEnter2D(Collision2D other)
    {
        CollisionEvent?.Invoke(other);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        TriggerEvent?.Invoke(other);
    }
}