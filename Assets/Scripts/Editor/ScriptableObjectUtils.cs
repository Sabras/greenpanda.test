﻿using System;
using UnityEditor;
using System.Reflection;
using UnityEngine;

public static class ScriptableObjectUtils
{
    [MenuItem("Assets/Create/Asset From Scriptable Object", false)]
    public static void CreateObjectAsAsset()
    {
        var activeObject = Selection.activeObject;
        var assetPath = AssetDatabase.GetAssetPath(activeObject);
        var monoScript = (MonoScript) AssetDatabase.LoadAssetAtPath(assetPath, typeof(MonoScript));
        if (monoScript == null)
            return;
        var scriptType = monoScript.GetClass();
        var path = EditorUtility.SaveFilePanelInProject("Save asset as .asset", "New " + scriptType.Name + ".asset",
            "asset", "Please enter a file name");

        if (path.Length == 0) return;
        try
        {
            var inst = ScriptableObject.CreateInstance(scriptType);
            AssetDatabase.CreateAsset(inst, path);
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = inst;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
}