﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField] private Text scoreText;

    private void OnEnable()
    {
        GlobalEventsHolder.ScoreChanged += UpdateScoreText;
    }

    private void Start()
    {
        UpdateScoreText(ScoreManager.Score);
    }

    private void UpdateScoreText(int score)
    {
        scoreText.text = score.ToString();
    }
}