﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private const string ScorePrefsKey = "Score";

    public static int Score
    {
        get { return PlayerPrefs.GetInt(ScorePrefsKey, 0); }
        set
        {
            PlayerPrefs.SetInt(ScorePrefsKey, value);
            GlobalEventsHolder.ScoreChanged?.Invoke(value);
        }
    }

    private void OnEnable()
    {
        GlobalEventsHolder.PizzaConsumed += ConsumePizzaAddScore;
    }

    private void ConsumePizzaAddScore(PizzaController pizzaController)
    {
        Score += pizzaController.GetPizzaScore();
    }
}