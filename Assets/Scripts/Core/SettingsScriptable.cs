﻿using UnityEngine;

public class SettingsScriptable : ScriptableObject
{
    [Header("Global")]
    public AudioClip MusicClip;

    public float PizzaSpawnDelay;
    public float PizzaSpawnBlockTimer;
    public int PizzaScore;
}




