﻿using System;

public static class GlobalEventsHolder
{
    public static Action<PizzaController> PizzaConsumed;
    public static Action SpawnPizza;
    public static Action SceneStartsLoading;
    public static Action<int> ScoreChanged;
}
